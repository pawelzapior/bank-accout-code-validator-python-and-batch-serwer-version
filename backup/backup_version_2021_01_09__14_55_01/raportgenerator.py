# -*- coding: utf-8 -*-

import os

def tlumacz(s):
    if("True" in s):
        return "poprawny"
    else:
        return "niepoprawny" 

raport = open("index.html","w")

fnames=os.listdir("out")

raport.write(f"""
             
<!DOCTYPE html>
<html lang="pl-PL">

<head>
<meta charset="utf-8">
<title> Bank Account Code Validator | Paweł Zapiór </title>
<link rel="stylesheet" href="style.css" type="text/css">
<meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc dignissim bibendum massa et tincidunt. Sed tincidunt lacus id eleifend mollis. In hac habitasse platea dictumst. Nullam eu urna condimentum elit tincidunt lacinia. Curabitur quis ex leo. Morbi posuere rutrum neque. Mauris aliquam nisl id massa fringilla, at facilisis mauris blandit. Duis nec porta lectus, eu congue ex. Curabitur semper tristique lorem sed blandit. Integer luctus nec lacus id interdum. Pellentesque a consequat dolor, sed ultrices ex.">
<meta name="author" content="Paweł Zapiór">
<link rel="stylesheet" href="style.css" type="text/css">
<link rel="stylesheet" href="css/fontello.css" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Staatliches&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Thasadith:400,700&amp;subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Righteous&amp;subset=latin-ext" rel="stylesheet">

<meta name="viewport"
content="width=device-width,
initial-scale=1.0">
</head>

<body>

<div id="container">
<header>

<div id="socialmedia">
<div style=float:left; class="option"> <p class="soc-option"> Hej, tu autor programu, Pawel! Odwiedz moje konto na GitLab! </p></div>
<div style=float:right; class="ikon-option"> <a href="https://gitlab.com/pawelzapior" target="_blank" > <i class="icon-gitlab"></i> </a></div>

<div style=clear:both;> </div>
</div>
<div id="logo">
<h1> <a href="index.html"> <i class="icon-panda_mark"></i> Bank Account Code Validator</a></h1>
</div>

<div id="nav">
<ul class="text">
<li> <a class="option" href="index.html">Wygenerowane dane</a></li>
<li> <a class="option" href="algorytm.html">O algorytmie</a></li>
</ul>
</div>

</header>

<div class="content">
    
    
    
    
    
    """)

for i in fnames:
    if(("outputBankAccountCode" in i) and (".txt" in i)):
        file=open(f"out/{i}","r")
        
        line = file.readline()        
        if(line=="False"):
            raport.write(f"""
                         <h2> Plik {i} </h2>

<p class="text"> <strong> Niestety, nie udalo sie wygenerowac poprawnego raportu dla tego numeru. Prawdopodobnie wpisales go w zlej formie. Sprawdz czy nie podales kodu kraju IBAN przed numerem (nie powinno go byc). Moze ilosc cyfr sie nie zgadza? Pamietaj, ze w Polsce standard to 26. Wprowadz poprawne dane i sproboj jeszcze raz.  </strong></p>
    
    
    """)
        else:
            finnumb=line
            zgfinnumb = tlumacz(file.readline())
            ctrl = file.readline() 
            zgncltr = tlumacz(file.readline()) 
            ibn = file.readline() 
            zgnibn = tlumacz(file.readline())
            bank = file.readline() 
            zgn = tlumacz(file.readline())
            raport.write(f"""
                         <h2> Plik {i} </h2>

<p class="text"> <strong> Obliczona cyfra kontrolna dla numeru instytucji finansowej to: {finnumb}  </strong></p>
<p class="text"> <strong> Jej zgodnosc z podanym numerem jest: {zgfinnumb}  </strong></p>    
<p class="text"> <strong> Obliczona wartosc sumy kontrolnej to: {ctrl}  </strong></p>
<p class="text"> <strong> Zgodnosc sumy kontolnej jest: {zgncltr}  </strong></p>
<p class="text"> <strong> Obliczona wartosc kontolna numeru IBAN to: {ibn}  </strong></p>
<p class="text"> <strong> Zgodnosc tej wartosci zatem jest: {zgnibn}  </strong></p>
<p class="text"> <strong> Rozpoznany bank z ktorego pochodzi ten numer konta to: {bank}  </strong></p>
<p class="text"> <strong> A zatem numer bankowy jest: {zgn}  </strong></p>
""")

raport.write(f"""
<img class="logoGF" src="panda.jpg" alt="Panda">             

             
</div> 
<div id="footer">

<p class="text">&copy; 2021 by Pawel Zapior </p>

</div>
</div>

</body>
</html>
    
    
    
    
    
    """)
    
raport.close()
os.startfile("index.html")