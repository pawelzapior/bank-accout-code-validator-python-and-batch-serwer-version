import os

class banknumber:
    n=[]
    
    def __init__(self,number):
        self.strA=number
        
    def toarray(self):
        tab=[]
        
        tabstr=list(self.strA)
        for i in tabstr:
            if(i not in ' '):
                tab.append(int(i))
        return tab
            
    def FinCtrlNumb(self):
        num=self.toarray()
        wag=3*num[2]+9*num[3] + 7*num[4] +1*num[5] +3*num[6]+ 9*num[7]+7*num[8]
        wart=wag%10
        if(wart!= 0 ):
            wart = 10-wart
            return wart
        else:
            return wart
    def Recogn(self):
        num=self.toarray()
        if(num[2]==1 and num[3]==0 and num[4]==1 and num[5]==0):
            return("Narodowy Bank Polski")
        elif(num[2]==1 and num[3]==0 and num[4]==5 and num[5]==0):
            return("ING Bank Śląski")
        elif(num[2]==1 and num[3]==0 and num[4]==2 and num[5]==0):
            return("PKO Bank Polski")
        elif(num[2]==1 and num[3]==0 and num[4]==6 and num[5]==0):
            return("Bank BPH")
        elif(num[2]==1 and num[3]==0 and num[4]==9 and num[5]==0):
            return("Santander Bank Polska")
        elif(num[2]==1 and num[3]==1 and num[4]==4 and num[5]==0):
            return("mBank")
        elif(num[2]==1 and num[3]==1 and num[4]==6 and num[5]==0):
            return("Bank Millennium")
        elif(num[2]==1 and num[3]==2 and num[4]==4 and num[5]==0):
            return("Pekao SA")
        elif(num[2]==1 and num[3]==3 and num[4]==2 and num[5]==0):
            return("Bank Pocztowy")
        elif(num[2]==1 and num[3]==7 and num[4]==5 and num[5]==0):
            return ("Raiffeisen Bank")
        else:
            return ("Not recognized")
    
    def ControlSum(self):
        num=self.toarray()
        sumactrl=[]
        for i in range(2,len(num)):
            sumactrl.append(str(num[i]))
        sumactrl.append("2")
        sumactrl.append("5")
        sumactrl.append("2")
        sumactrl.append("1")
        sumactrl.append("0")
        sumactrl.append("0")
            
        bigsuma=int(("".join(sumactrl)))
        wynsum=98-(bigsuma%97)
        
        return wynsum
    
    def IBANcontrol(self):
        num=self.toarray()
        sumactrl=[]
        for i in range(2,len(num)):
            sumactrl.append(str(num[i]))
        sumactrl.append("2")
        sumactrl.append("5")
        sumactrl.append("2")
        sumactrl.append("1")
        sumactrl.append(str(num[0]))
        sumactrl.append(str(num[1]))
            
        bigsuma=int(("".join(sumactrl)))
        wynsum=bigsuma%97
        
        return wynsum
        
    


fnames=os.listdir("in")

for i in fnames:
    if(("BankAccountCode" in i) and (".txt" in i)):
        finp=open(f"in/{i}","r")
        fout=open(f"out/output{i}","w+")  
        
        i=finp.readline()
        obj=banknumber(i)
        
        try:
            tab=obj.toarray()
            if(len(tab)==26):                
                
                fcn=obj.FinCtrlNumb()
                cs=obj.ControlSum()
                bank=obj.Recogn()
                ibn=obj.IBANcontrol()
                check=False
                if ibn==1:
                    if cs==(tab[0]*10+tab[1]):
                        if fcn==tab[9]:
                            check=True
                
                print(check)
    
                fout.writelines(str(fcn)+"\n")
                fout.writelines(str(fcn==tab[9])+"\n")
                fout.writelines(str(cs)+"\n")
                fout.writelines(str(cs==(tab[0]*10+tab[1]))+"\n")
                fout.writelines(str(ibn)+"\n")                
                fout.writelines(str(ibn==1)+"\n") 
                fout.writelines(str(bank)+"\n")
                fout.writelines(str(check)+"\n")                 
            else:
                fout.write(False)    
        except:
            fout.write(str(False))
            
            
        finp.close()
        fout.close()


    
   # f=open("")