@echo off
color 70

:zero
cls
echo  /--------\ Bank Account Code Validator: /---------\ 
echo /---------------------\ Menu: /---------------------\
echo.
echo 1. Rozpocznij dzialanie skryptu
echo 2. Wypisz informacje o programie
echo 3. Wykonaj kopie zapasowa
echo 4. Zakoncz dzialanie programu
echo.
echo \--------------\       Pawel Zapior    /--------------/ 
echo  \--------------\Jezyki Skryptowe 2020/--------------/
echo.
echo.
type logo.txt
echo.
set /p choice=Podaj cyfre: 
if %choice%==1 goto jeden
if %choice%==2 goto dwa
if %choice%==3 goto trzy
if %choice%==4 goto cztery
Else echo Niepoprawna wartosc
goto zero

:jeden
cls
echo  /--------\ Bank Account Code Validator: /---------\ 
echo.
echo Uruchomiono program. Zaraz zobaczysz raport.
echo.
echo.
type logo.txt
echo.
echo \--------------\       Pawel Zapior    /--------------/ 
echo  \--------------\Jezyki Skryptowe 2020/--------------/

del out\*.txt
call validator.py
call raportgenerator.py

goto zero

:dwa 
cls
echo /---------------------\ Informacje /---------------------\
echo Autor: Pawel Zapior
echo.
echo Dzialanie: 
echo Program pozwala na walidacje polskich numerow kont bankowych. Generuje raport html w ktorym okresla z jakiego banku pochodzi numer oraz czy jest poprawny poprzez walidacje zawartych w nim sum kontrolnych. 
echo.
echo Cel:
echo Program wykonany zostal jako projekt z przedmiotu jezyki skryptowe 2020 realizowanego na Wydziale Matematyki Stosowanej Politechniki Slaskiej. Podziekowaniowy Asciirix dla prof. Marcina Wozniaka za poprowadzone zajecia.
echo.
echo.
echo 
type asteriks.txt
echo.
echo \---------------\       Pawel Zapior     /--------------/ 
echo  \---------------\Jezyki Skryptowe 2020 /--------------/
pause
goto zero

:trzy
cls
echo  /--------\ Bank Account Code Validator: /---------\ 
echo.
set datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%__%time:~0,2%_%time:~3,2%_%time:~6,2%
xcopy /s /i /y /q ..\source ..\backup\backup_version_%datetimef%
echo Wykonano backup w folderze projetku.
echo Plikow szukaj w folderze /backup dla dzisiejszej daty i godziny: %datetimef%
echo.
echo.
type logo.txt
echo.
echo \--------------\       Pawel Zapior    /--------------/ 
echo  \--------------\Jezyki Skryptowe 2020/--------------/
pause
goto zero

:cztery
cls
echo Dziekuje za skozystanie z programu!
echo.
echo.
type logo.txt 
echo.
echo.
pause
exit
